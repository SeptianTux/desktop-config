#!/bin/bash

function chkPrv() {
	if [ $EUID -eq 0 ]; then
		if [ $HOME == '/root' ]; then
			echo "Run as superuser account!."
			exit 1
		else
			echo "working..."
		fi
	else
		echo "Use sudo, please!"
		exit 1
	fi
}

function thunarCfg() {
	if [ ! -d $HOME/.config/xfce4/xfconf/xfce-perchannel-xml ]; then
		mkdir $HOME/.config/xfce4/xfconf/xfce-perchannel-xml
		chmod 755 $HOME/.config/xfce4/xfconf/xfce-perchannel-xml
		chown $USER:$USER $HOME/.config/xfce4/xfconf/xfce-perchannel-xml
	fi

	conf_file="$HOME/.config/xfce4/xfconf/xfce-perchannel-xml/thunar.xml"
	rep_ln=`cat $conf_file | grep "last-menubar-visible" -n -o | grep -Eo "[[:digit:]]*"`

	cfg_val='  <property name="last-menubar-visible" type="bool" value="false"\/>'

	if [ -z $rep_ln ]; then
		sed -i "s/<\/channel>/$cfg_val\n<\/channel>/" $conf_file
	else
		sed -i $rep_ln"s/.*/$cfg_val/" $conf_file
	fi

	chown $USER:$USER "$conf_file"

	unset conf_file rep_ln cfg_val
}

function xfce4NotifyCfg() {
	if [ ! -d $HOME/.config/xfce4/xfconf/xfce-perchannel-xml ]; then
		mkdir $HOME/.config/xfce4/xfconf/xfce-perchannel-xml
		chmod 755 $HOME/.config/xfce4/xfconf/xfce-perchannel-xml
		chown $USER:$USER $HOME/.config/xfce4/xfconf/xfce-perchannel-xml
	fi

	conf_file="$HOME/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-notifyd.xml"
	rep_ln=`cat $conf_file | grep "theme" -n -o | grep -Eo "[[:digit:]]*"`

	cfg_val='  <property name="theme" type="string" value="tynI3-OM2048-ARCDRK"\/>'

	if [ -z $rep_ln ]; then
		sed -i "s/<\/channel>/$cfg_val\n<\/channel>/" $conf_file
	else
		sed -i $rep_ln"s/.*/$cfg_val/" $conf_file
	fi

	chown $USER:$USER "$conf_file"

	unset conf_file rep_ln cfg_val
}

function gpicCfg() {
	if [ ! -d $HOME/.config/gpicview ]; then
		mkdir $HOME/.config/gpicview
		chmod 755 $HOME/.config/gpicview
		chown $USER:$USER $HOME/.config/gpicview
	fi

	conf="$HOME/.config/gpicview/gpicview.conf"
	bg_ln=`cat "$conf" | grep "bg=" -n -o | grep "[0-9]" -o`
	bg_full_ln=`cat $conf | grep "bg_full=" -n -o | grep "[0-9]*" -o`

	color="#404552"

	sed -i ""$bg_ln"s/bg=#[a-zA-Z0-9]*/bg=$color/" $conf
	sed -i ""$bg_full_ln"s/bg_full=#[a-zA-Z0-9]*/bg_full=$color/" $conf

	chown $USER:$USER "$conf"

	unset color conf bg_ln bg_full_ln
}

function comptonCfg() {
	cat ./tynI3-OM2048-FDK/etc/xdg/compton.conf > /etc/xdg/compton.conf
}

function terminalCfg() {
	cat ./tynI3-OM2048-FDK/home/user/.config/xfce4/terminal/terminal.rc > $HOME/.config/xfce4/terminal/terminalrc
}

function xinitCfg() {
	cat ./tynI3-OM2048-FDK/home/user/.xinitrc > $HOME/.xinitrc
}

function fehConf() {
	wp=`cat ./tynI3-OM2048-FDK/home/user/.fehbg | grep -o "'/[a-zA-Z0-9\/]*.*" | sed "s/'//g"`
	
	cp $wp $HOME/Pictures
	chmod 644 $HOME/Picture/$(basename $wp)
	chown $USER:$USER $HOME/Picture/$(basename $wp)
}

function i3Conf() {
	if [ ! -d $HOME/.i3 ]; then
		mkdir $HOME/.i3
		chmod 755 $HOME/.i3
		chown $USER:$USER $HOME/.i3
	fi

	cp ./tynI3-OM2048-FDK/home/user/.i3/config $HOME/.i3/
	cp ./tynI3-OM2048-FDK/home/user/.i3/i3status.sh $HOME/.i3/
	chown -R $USER:$USER $HOME/.i3
	chmod 755 $HOME/.i3/i3status.sh
	chmod 644 $HOME/.i3/config
}

function gtk2Conf() {
	cp ./tynI3-OM2048-FDK/home/user/.gtkrc-2.0 $HOME
	chown $USER:$USER $HOME/.gtkrc-2.0
	chmod 644 $HOME/.gtkrc-2.0
	cp ./tynI3-OM2048-FDK/home/user/.gtkrc-2.0 /root
	chown root:root /root/.gtkrc-2.0
	chmod 644 /root/.gtkrc-2.0
}

function fire() {

	chkPriv

	echo "configuring thunar file manager..."
	thunarCfg
	echo "configuring xfce4 notify..."
	xfce4NotifyCfg
	echo "configuring gpic image viewer..."
	gpicCfg
	echo "configuring compton compositor..."
	comptonCfg
	echo "configuring xfce4 terminal..."
	terminalCfg
	echo "getting xinit config"
	xinitCfg
	echo "getting feh config..."
	fehConf
	echo "getting i3 config..."
	i3Conf
	echo "getting gtk2 config..."
	gtk2Conf

	echo "done..."

}

fire
