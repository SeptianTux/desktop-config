#!/bin/bash

list_file="$0.list"
current_color_start="#dbdbdb"
current_color_stop="#757575"
new_color_start="#7E7E9C"
new_color_stop="#9090AC"

function getFileList() {
  find $PWD -type f -name "*.svg" > $list_file
}

function getFileName() {
    sed "$1!d" $list_file
}

function getTotalLineList() {
    cat $list_file | wc -l
}

function replaceColor() {
    sed -i "s/$current_color_start/$new_color_start/g" `getFileName $1`
    sed -i "s/$current_color_stop/$new_color_stop/g" `getFileName $1`
}

function loopMessage() {
    echo "changing color of `getFileName $1` ..."
}

function looping() {
    start=1
    stop=`getTotalLineList`

    while [ $start -le $stop ]; do
        loopMessage $start
        replaceColor $start

        let start=$start+1
    done
}

case $1 in
    --get-list ) getFileList
        ;;
    --run ) looping
        ;;
esac
