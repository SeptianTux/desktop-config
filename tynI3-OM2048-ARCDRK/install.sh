#!/bin/bash

_USER=$1

function chkPriv() {
	if [ $EUID -eq 0 ]; then
		if [ /home/$_USER == '/root' ]; then
			echo "Run as superuser account!."
			exit 1
		else
			echo "working..."
		fi
	else
		echo "Use sudo, please!"
		exit 1
	fi
}

function thunarCfg() {
	if [ ! -d /home/$_USER/.config/xfce4/xfconf/xfce-perchannel-xml ]; then
		mkdir -p /home/$_USER/.config/xfce4/xfconf/xfce-perchannel-xml
		chown -R $_USER:$_USER /home/$_USER/.config/xfce4
	fi

	conf_file="/home/$_USER/.config/xfce4/xfconf/xfce-perchannel-xml/thunar.xml"
	rep_ln=`cat $conf_file | grep "last-menubar-visible" -n -o | grep -Eo "[[:digit:]]*"`

	cfg_val='  <property name="last-menubar-visible" type="bool" value="false"\/>'

	if [ -z $rep_ln ]; then
		sed -i "s/<\/channel>/$cfg_val\n<\/channel>/" $conf_file
	else
		sed -i $rep_ln"s/.*/$cfg_val/" $conf_file
	fi

	chown $_USER:$_USER "$conf_file"

	unset conf_file rep_ln cfg_val
}

function xfce4NotifyCfg() {
	if [ ! -d /home/$_USER/.config/xfce4/xfconf/xfce-perchannel-xml ]; then
		mkdir -p /home/$_USER/.config/xfce4/xfconf/xfce-perchannel-xml
		chmod 755 /home/$_USER/.config/xfce4/xfconf/xfce-perchannel-xml
		chown $_USER:$_USER /home/$_USER/.config/xfce4/xfconf/xfce-perchannel-xml
	fi

	conf_file="/home/$_USER/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-notifyd.xml"
	rep_ln=`cat $conf_file | grep "theme" -n -o | grep -Eo "[[:digit:]]*"`

	cfg_val='  <property name="theme" type="string" value="tynI3-OM2048-ARCDRK"\/>'

	if [ -z $rep_ln ]; then
		sed -i "s/<\/channel>/$cfg_val\n<\/channel>/" $conf_file
	else
		sed -i $rep_ln"s/.*/$cfg_val/" $conf_file
	fi

	chown $_USER:$_USER "$conf_file"

	unset conf_file rep_ln cfg_val
}

function gpicCfg() {
	if [ ! -d /home/$_USER/.config/gpicview ]; then
		mkdir /home/$_USER/.config/gpicview
		chmod 755 /home/$_USER/.config/gpicview
		chown $_USER:$_USER /home/$_USER/.config/gpicview
	fi

	conf="/home/$_USER/.config/gpicview/gpicview.conf"
	bg_ln=`cat "$conf" | grep "bg=" -n -o | grep "[0-9]" -o`
	bg_full_ln=`cat $conf | grep "bg_full=" -n -o | grep "[0-9]*" -o`

	color="#404552"

	sed -i ""$bg_ln"s/bg=#[a-zA-Z0-9]*/bg=$color/" $conf
	sed -i ""$bg_full_ln"s/bg_full=#[a-zA-Z0-9]*/bg_full=$color/" $conf

	chown $_USER:$_USER "$conf"

	unset color conf bg_ln bg_full_ln
}

function comptonCfg() {
	cat ./etc/xdg/compton.conf > /etc/xdg/compton.conf
	chmod 664 /etc/xdg/compton.conf
	chwon root:root /etc/xdg/compton.conf
}

function terminalCfg() {
	if [ ! -d /home/$_USER/.config/xfce4/terminal ]; then
		mkdir -p /home/$_USER/.config/xfce4/terminal
		#sini
	cat ./home/user/.config/xfce4/terminal/terminalrc > /home/$_USER/.config/xfce4/terminal/terminalrc
}

function xinitCfg() {
	cat ./home/user/.xinitrc > /home/$_USER/.xinitrc
}

function fehConf() {
	wp=`cat ./home/user/.fehbg | grep -o "'/[a-zA-Z0-9\/]*.*" | sed "s/'//g"`
	
	cp $wp /home/$_USER/Pictures
	chmod 644 /home/$_USER/Picture/$(basename $wp)
	chown $_USER:$_USER /home/$_USER/Picture/$(basename $wp)
}

function i3Conf() {
	if [ ! -d /home/$_USER/.i3 ]; then
		mkdir /home/$_USER/.i3
		chmod 755 /home/$_USER/.i3
		chown $_USER:$_USER /home/$_USER/.i3
	fi

	cp ./home/user/.i3/config /home/$_USER/.i3/
	cp ./home/user/.i3/i3status.sh /home/$_USER/.i3/
	chown -R $_USER:$_USER /home/$_USER/.i3
	chmod 755 /home/$_USER/.i3/i3status.sh
	chmod 644 /home/$_USER/.i3/config
}

function gtk2Conf() {
	cp ./home/user/.gtkrc-2.0 /home/$_USER
	chown $_USER:$_USER /home/$_USER/.gtkrc-2.0
	chmod 644 /home/$_USER/.gtkrc-2.0
	cp ./home/user/.gtkrc-2.0 /root
	chown root:root /root/.gtkrc-2.0
	chmod 644 /root/.gtkrc-2.0
}

function fire() {

	chkPriv

	echo "configuring thunar file manager..."
	thunarCfg
	echo "configuring xfce4 notify..."
	xfce4NotifyCfg
	echo "configuring gpic image viewer..."
	gpicCfg
	echo "configuring compton compositor..."
	comptonCfg
	echo "configuring xfce4 terminal..."
	terminalCfg
	echo "getting xinit config"
	xinitCfg
	echo "getting feh config..."
	fehConf
	echo "getting i3 config..."
	i3Conf
	echo "getting gtk2 config..."
	gtk2Conf

	echo "done..."

}

fire
