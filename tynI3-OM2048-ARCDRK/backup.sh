#!/bin/bash

if [ ! -d ./home ]; then
	mkdir -pv ./home/user/{.config/xfce4/terminal,.conky,.i3,Pictures}
fi

if [ ! -d ./usr ]; then
	mkdir -pv ./usr/share/{themes,icons}
fi

if [ ! -d ./etc ]; then
	mkdir -pv ./etc/xdg
fi

cp -v $HOME/.config/xfce4/terminal/terminalrc							./home/user/.config/xfce4/terminal/
cp -v $HOME/.conky/conkyrc_i3status										./home/user/.conky/
cp -v $HOME/.i3/config													./home/user/.i3
cp -v $HOME/.i3/i3status.sh												./home/user/.i3
cp -v $HOME/.fehbg														./home/user/
cp -v $HOME/.xinitrc													./home/user/
cp -v $HOME/.gtkrc-2.0													./home/user/
cp -v `cat $HOME/.fehbg | grep -o "'/[a-zA-Z0-9\/]*.*" | sed "s/'//g"`	./home/user/Pictures/

cp -Rv /usr/share/themes/Arc-Dark										./usr/share/themes/
cp -Rv /usr/share/icons/Papirus-Dark									./usr/share/icons/

cp -Rv /etc/xdg/compton.conf											./etc/xdg/
